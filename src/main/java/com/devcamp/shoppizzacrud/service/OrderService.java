package com.devcamp.shoppizzacrud.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shoppizzacrud.model.Order;
import com.devcamp.shoppizzacrud.repository.IOrderRepository;

@Service
public class OrderService {
    @Autowired
    private IOrderRepository orderRepository;

    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    public Optional<Order> getOrderById(int id) {
        return orderRepository.findById(id);
    }

    public Order createOrder(Order order) {
        return orderRepository.save(order);
    }

    public Optional<Order> updateOrderById(int id, Order updatedOrder) {
        Optional<Order> orderData = orderRepository.findById(id);

        if (orderData.isPresent()) {
            Order order = orderData.get();
            order.setOrderDate(updatedOrder.getOrderDate());
            order.setRequiredDate(updatedOrder.getRequiredDate());
            order.setShippedDate(updatedOrder.getShippedDate());
            order.setStatus(updatedOrder.getStatus());
            order.setOrderDetails(updatedOrder.getOrderDetails());
            return Optional.of(orderRepository.save(order));
        } else {
            return Optional.empty();
        }
    }

    public void deleteOrderById(int id) {
        orderRepository.deleteById(id);
    }
}

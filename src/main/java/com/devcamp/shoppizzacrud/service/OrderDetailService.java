package com.devcamp.shoppizzacrud.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shoppizzacrud.model.OrderDetail;
import com.devcamp.shoppizzacrud.repository.IOrderDetailRepository;

@Service
public class OrderDetailService {
    @Autowired
    private IOrderDetailRepository orderDetailRepository;

    public List<OrderDetail> getAllOrderDetails() {
        return orderDetailRepository.findAll();
    }

    public Optional<OrderDetail> getOrderDetailById(int id) {
        return orderDetailRepository.findById(id);
    }

    public OrderDetail createOrderDetail(OrderDetail orderDetail) {
        return orderDetailRepository.save(orderDetail);
    }

    public Optional<OrderDetail> updateOrderDetailById(int id, OrderDetail updatedOrderDetail) {
        Optional<OrderDetail> orderDetailData = orderDetailRepository.findById(id);

        if (orderDetailData.isPresent()) {
            OrderDetail orderDetail = orderDetailData.get();
            orderDetail.setQuantityOrder(updatedOrderDetail.getQuantityOrder());
            orderDetail.setPriceEach(updatedOrderDetail.getPriceEach());
            return Optional.of(orderDetailRepository.save(orderDetail));
        } else {
            return Optional.empty();
        }
    }

    public void deleteOrderDetailById(int id) {
        orderDetailRepository.deleteById(id);
    }
}

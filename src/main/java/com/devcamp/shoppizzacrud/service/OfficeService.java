package com.devcamp.shoppizzacrud.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shoppizzacrud.model.Office;
import com.devcamp.shoppizzacrud.repository.IOfficeRepository;

@Service
public class OfficeService {
    @Autowired
    private IOfficeRepository officeRepository;

    public List<Office> getAllOffices() {
        return officeRepository.findAll();
    }

    public Optional<Office> getOfficeById(int id) {
        return officeRepository.findById(id);
    }

    public Office createOffice(Office office) {
        return officeRepository.save(office);
    }

    public Optional<Office> updateOfficeById(int id, Office updatedOffice) {
        Optional<Office> officeData = officeRepository.findById(id);

        if (officeData.isPresent()) {
            Office office = officeData.get();
            office.setCity(updatedOffice.getCity());
            office.setPhone(updatedOffice.getPhone());
            office.setAddressLine(updatedOffice.getAddressLine());
            office.setState(updatedOffice.getState());
            office.setCountry(updatedOffice.getCountry());
            office.setTerritory(updatedOffice.getTerritory());
            return Optional.of(officeRepository.save(office));
        } else {
            return Optional.empty();
        }
    }

    public void deleteOfficeById(int id) {
        officeRepository.deleteById(id);
    }
}

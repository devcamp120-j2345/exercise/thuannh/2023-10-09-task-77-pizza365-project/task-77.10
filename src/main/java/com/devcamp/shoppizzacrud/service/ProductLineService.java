package com.devcamp.shoppizzacrud.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.shoppizzacrud.model.ProductLine;
import com.devcamp.shoppizzacrud.repository.IProductLineReposiroty;

@Service
public class ProductLineService {
    @Autowired
    private IProductLineReposiroty productLineRepository;

    public List<ProductLine> getAllProductLines() {
        return productLineRepository.findAll();
    }

    public Optional<ProductLine> getProductLineById(int id) {
        return productLineRepository.findById(id);
    }

    public ResponseEntity<Object> createProductLine(ProductLine paramProductLine) {
        try {
            ProductLine vProductLine = new ProductLine();
            vProductLine.setProductLine(paramProductLine.getProductLine());
            vProductLine.setDescription(paramProductLine.getDescription());
            ProductLine vProductLineSave = productLineRepository.save(vProductLine);
            return new ResponseEntity<>(vProductLineSave, HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified ProductLine: " + e.getCause().getCause().getMessage());
        }
    }

    public ResponseEntity<Object> updateProductLine(Integer id, ProductLine paramProductLine) {
        Optional<ProductLine> vProductLineData = productLineRepository.findById(id);
        if (vProductLineData.isPresent()) {
            try {
                ProductLine vProductLine = vProductLineData.get();
                vProductLine.setProductLine(paramProductLine.getProductLine());
                vProductLine.setDescription(paramProductLine.getDescription());
                ProductLine vProductLineSave = productLineRepository.save(vProductLine);
                return new ResponseEntity<>(vProductLineSave, HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified ProductLine: " + e.getCause().getCause().getMessage());
            }
        } else {
            ProductLine vProductLineNull = new ProductLine();
            return new ResponseEntity<>(vProductLineNull, HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<Object> deleteProductLineById(Integer id) {
        Optional<ProductLine> vProductLineData = productLineRepository.findById(id);
        if (vProductLineData.isPresent()) {
            try {
                productLineRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            ProductLine vProductLineNull = new ProductLine();
            return new ResponseEntity<>(vProductLineNull, HttpStatus.NOT_FOUND);
        }
    }
}

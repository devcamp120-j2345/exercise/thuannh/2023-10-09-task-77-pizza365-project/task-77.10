package com.devcamp.shoppizzacrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shoppizzacrud.model.ProductLine;

public interface IProductLineReposiroty extends JpaRepository<ProductLine, Integer> {
    
}

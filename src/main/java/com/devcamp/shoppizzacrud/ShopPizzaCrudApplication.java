package com.devcamp.shoppizzacrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopPizzaCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShopPizzaCrudApplication.class, args);
	}

}

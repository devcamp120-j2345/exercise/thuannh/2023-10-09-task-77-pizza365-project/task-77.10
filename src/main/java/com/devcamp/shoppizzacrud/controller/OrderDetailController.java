package com.devcamp.shoppizzacrud.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shoppizzacrud.model.OrderDetail;
import com.devcamp.shoppizzacrud.service.OrderDetailService;

@CrossOrigin
@RestController
@RequestMapping("/order-details")
public class OrderDetailController {
    @Autowired
    private OrderDetailService orderService;

    @GetMapping
    public ResponseEntity<List<OrderDetail>> getAllOrderDetails() {
        try {
            List<OrderDetail> orderDetails = orderService.getAllOrderDetails();
            return new ResponseEntity<>(orderDetails, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrderDetail> getOrderDetailById(@PathVariable("id") int id) {
        try {
            Optional<OrderDetail> orderDetailData = orderService.getOrderDetailById(id);
            if (orderDetailData.isPresent()) {
                return new ResponseEntity<>(orderDetailData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping
    public ResponseEntity<OrderDetail> createOrderDetail(@Valid @RequestBody OrderDetail orderDetail) {
        try {
            OrderDetail createdOrderDetail = orderService.createOrderDetail(orderDetail);
            return new ResponseEntity<>(createdOrderDetail, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<OrderDetail> updateOrderDetailById(@PathVariable("id") int id,
            @Valid @RequestBody OrderDetail updatedOrderDetail) {
        try {
            Optional<OrderDetail> updatedOrderDetailData = orderService.updateOrderDetailById(id, updatedOrderDetail);
            if (updatedOrderDetailData.isPresent()) {
                return new ResponseEntity<>(updatedOrderDetailData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteOrderDetailById(@PathVariable("id") int id) {
        try {
            orderService.deleteOrderDetailById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

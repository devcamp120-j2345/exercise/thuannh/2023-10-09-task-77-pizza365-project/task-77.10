package com.devcamp.shoppizzacrud.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shoppizzacrud.model.Office;
import com.devcamp.shoppizzacrud.service.OfficeService;

@CrossOrigin
@RestController
@RequestMapping("/offices")
public class OfficeController {
    @Autowired
    private OfficeService officeService;

    @GetMapping
    public ResponseEntity<List<Office>> getAllOffices() {
        try {
            List<Office> offices = officeService.getAllOffices();
            return new ResponseEntity<>(offices, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Office> getOfficeById(@PathVariable("id") int id) {
        try {
            Optional<Office> officeData = officeService.getOfficeById(id);
            if (officeData.isPresent()) {
                return new ResponseEntity<>(officeData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping
    public ResponseEntity<Office> createOffice(@Valid @RequestBody Office office) {
        try {
            Office createdOffice = officeService.createOffice(office);
            return new ResponseEntity<>(createdOffice, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Office> updateOfficeById(@PathVariable("id") int id,
            @Valid @RequestBody Office updatedOffice) {
        try {
            Optional<Office> updatedOfficeData = officeService.updateOfficeById(id, updatedOffice);
            if (updatedOfficeData.isPresent()) {
                return new ResponseEntity<>(updatedOfficeData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteOfficeById(@PathVariable("id") int id) {
        try {
            officeService.deleteOfficeById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
